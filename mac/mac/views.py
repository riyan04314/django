from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from django.contrib.auth import  authenticate, login, logout
from django.contrib import messages
import random

def home(request):
    pagename = 'Home Page'
    fname = request.POST.get('fname','Robin Hood')
    curtime=785495
    request.session['curtime']=curtime
    data = {'pagename':pagename,'fname':fname, 'coin_name':'Bitcoin', 'price':7845,'curtime':curtime}


    return render(request,'index.html',data)

def about(request):
    username = random.randrange(9,100)
    #randint(9,100)
    if(request.session.has_key('curtime')):
        curtime=request.session.get('curtime')
        username='ram'+str(username)
        email=str(username)+'@gmail.com'
        pwd=14521452
        myuser=User.objects.create_user(username,email,pwd)
        myuser.first_name='Rama'
        myuser.last_name='Ray'
        myuser.save()
        messages.success(request,"Accout created")
        user=authenticate(username=username,password=pwd)
        if user is not None:
            login(request,user)
        else:
            return redirect('home')

    data = {'pagename':'AboutUS Page','curtime':username}
    return render(request,'about.html',data)

def contact(request):
    try:
        del request.session['curtime']
    except:
        pass

    return HttpResponse("Contact US...")
