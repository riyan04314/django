from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    pagename = 'Blog Page'
    fname = request.POST.get('fname','Robin Hood')
    data = {'pagename':pagename,'fname':fname, 'coin_name':'Bitcoin', 'price':7845}
    return render(request,'index.html',data)
